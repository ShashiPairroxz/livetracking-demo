//
//  MyLiveActivityWidgetBundle.swift
//  MyLiveActivityWidget
//
//  Created by Shashi Gupta on 04/06/24.
//

import WidgetKit
import SwiftUI

@main
struct MyLiveActivityWidgetBundle: WidgetBundle {
    var body: some Widget {
        MyLiveActivityWidget()
    }
}
