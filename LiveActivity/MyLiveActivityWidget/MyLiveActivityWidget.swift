//
//  MyLiveActivityWidget.swift
//  MyLiveActivityWidget
//
//  Created by Shashi Gupta on 04/06/24.
//

import WidgetKit
import SwiftUI
import ActivityKit

struct MyLiveActivityWidget: Widget {
    
    var body: some WidgetConfiguration{
        ActivityConfiguration(for: TimerAttributes.self) { context in
            TrackingWidgetView(context: context)
        } dynamicIsland: { context in
            DynamicIsland {
                DynamicIslandExpandedRegion(.leading) {
                    Text("Main")
                }
            } compactLeading: {
                Text("CL")
            } compactTrailing: {
                Text("CT")
            } minimal: {
                Text("CM")
            }
            
        }
    }
}

struct TrackingWidgetView:View{
    
    var context : ActivityViewContext<TimerAttributes>
    
    var body: some View{
        
        VStack{
            HStack{
                Text("Brown Sugar")
                    .font(.system(size: 15, weight: .light))
                    .foregroundColor(.gray)
                Spacer()
                Text("Zomato")
                    .font(.system(size: 17, weight: .bold))
                    .foregroundColor(.black)
            }
            .padding(10)
            VStack{
                Text("\(context.state.title) #\(context.attributes.orderNumber)")
                    .font(.system(size: 18, weight: .semibold))
                    .foregroundColor(.black)
                    .frame(maxWidth: .infinity, alignment: .leading)
                    .padding(.leading, 10)
                HStack{
                    Text("On time")
                        .font(.system(size: 15, weight: .regular))
                        .foregroundColor(.green)
                    Text("|")
                        .font(.system(size: 15, weight: .regular))
                        .foregroundColor(.black)
                    Text(context.state.subTitle)
                        .font(.system(size: 15, weight: .regular))
                        .foregroundColor(.black)
                    
                }
                .frame(maxWidth: .infinity, alignment: .leading)
                .padding(.leading, 10)
                .padding(.top, 4)
                
                ZStack{
                    
                    ProgressView(value: context.state.progress)
                        .progressViewStyle(LinearProgressViewStyle())
                        .padding(.leading, 10)
                        .padding(.trailing, 10)
                    
                    HStack{
                        Image(systemName: "bicycle.circle.fill")
                            .resizable()
                            .scaledToFit()
                            .frame(width: 30, height: 30)
                            .padding(.leading, 10)
                            .background(.white)
                        Spacer()
                        Image(systemName: "fork.knife.circle.fill")
                            .resizable()
                            .scaledToFit()
                            .frame(width: 30, height: 30)
                            .background(.white)
                        Spacer()
                        Image(systemName: "house.circle.fill")
                            .resizable()
                            .scaledToFit()
                            .frame(width: 30, height: 30)
                            .padding(.trailing, 10)
                            .background(.white)
                    }
                    
                    .frame(maxWidth: .infinity, alignment: .leading)
                }
                
            }
            
            .padding(.bottom, 10)
        }
        
    }
}
