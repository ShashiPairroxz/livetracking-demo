//
//  TimerAttributes.swift
//  LiveActivity
//
//  Created by Shashi Gupta on 04/06/24.
//

import Foundation
import ActivityKit
import SwiftUI

struct TimerAttributes: ActivityAttributes {
    
    public typealias TimerStatus = ContentState
    
    public struct ContentState: Codable, Hashable {
        var progress: Double
        var title:String
        var subTitle:String
    }

    var orderNumber: String
}
