//
//  ViewController.swift
//  LiveActivity
//
//  Created by Shashi Gupta on 04/06/24.
//

import UIKit
import ActivityKit

class ViewController: UIViewController {
    
    var liveActivity: Activity<TimerAttributes>?
    var progress = 0.0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        startLiveActivity()
    }
    
    func startLiveActivity() {
        self.progress = 0
        let initialContentState = TimerAttributes.ContentState(progress: self.progress, title: "Order Received", subTitle: "Driver reaching to the restaurant")
        let activityAttributes = TimerAttributes(orderNumber: "9876765")
        
        do {
            liveActivity = try Activity<TimerAttributes>.request(
                attributes: activityAttributes,
                contentState: initialContentState,
                pushType: nil
            )
            print("Requested a live activity with id: \(String(describing: liveActivity?.id))")
        } catch (let error) {
            print("Error requesting live activity \(error.localizedDescription)")
        }
    }
    
    func updateLiveActivity() {
        guard let activity = liveActivity else { return }
        self.progress += 0.5
        print("d asd asd as d\(progress)")
        if progress == 0.5{
            let updatedContentState = TimerAttributes.ContentState(progress: self.progress, title: "Food preparing", subTitle: "Arriving in 30 minutes")
            Task {
                await activity.update(using: updatedContentState)
            }
        }
        else if progress == 1.0{
            let updatedContentState = TimerAttributes.ContentState(progress: self.progress, title: "Delivered Order", subTitle: "Arrived at your doorsteps.")
            Task {
                await activity.update(using: updatedContentState)
            }
        }
        
    }
    
    func endLiveActivity() {
        guard let activity = liveActivity else { return }
        Task {
            await activity.end(dismissalPolicy: .immediate)
        }
    }
    
    
    @IBAction func startButtonTapped(_ sender: UIButton) {
        startLiveActivity()
    }
    
    @IBAction func updateButtonTapped(_ sender: UIButton) {
        updateLiveActivity()
    }
    
    @IBAction func endButtonTapped(_ sender: UIButton) {
        endLiveActivity()
    }
}
